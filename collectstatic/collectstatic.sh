#!/bin/sh
set -e

#################################
# create release folder structure
# TIMESTAMP=%date:~4,2%%date:~7,2%%date:~10,4%_%time:~0,2%%time:~3,2%%time:~6,2%
TIMESTAMP=$(date +"%m-%d-%y")_$(date +"%H-%M-%S")
FOLDER=release_bundle_$TIMESTAMP
ROOT_FOLDER=$FOLDER/sln_editor
SRC_FOLDER=$ROOT_FOLDER/src
STYLE_FOLDER=$ROOT_FOLDER/style
DEST_FOLDER=../django-server/sln_editor/static
mkdir $FOLDER
mkdir $ROOT_FOLDER
mkdir $SRC_FOLDER
mkdir $STYLE_FOLDER
echo "Made folder" $FOLDER

#################################
# copy static files to repo
cp -a ./static/* $FOLDER/.

#################################
# build Scriptblocks and widgets package
cd ../../scriptblocks
java -jar plovr.jar build src/weblogo-compiled-config.js > scriptblocks.js
cd ../clix/collectstatic
cp -a ../../scriptblocks/scriptblocks.js $SRC_FOLDER

#################################
# admin folder in current release bundles isn't in the repo! So ignoring it for now.

#################################
cp -a ../../wlwidgets/style/widgets.css $STYLE_FOLDER
mkdir $SRC_FOLDER/jscolor
cp -a ../../wlwidgets/libs/jscolor.min.js $SRC_FOLDER/jscolor/.
mkdir $SRC_FOLDER/dygraph
cp -a ../../wlwidgets/libs/dygraph*.* $SRC_FOLDER/dygraph/.
mkdir $STYLE_FOLDER/font-awesome
cp -a ../../wlwidgets/style/font-awesome-4.7.0/* $STYLE_FOLDER/font-awesome/.


##
cp -a ../../weblogo_threejs $SRC_FOLDER/weblogo_threejs
rm -rf $FOLDER/weblogo_threejs/.git
rm -rf $FOLDER/weblogo_threejs/obj

# cp -a ../../WLSite/src/django-cms/cms/static/cms $ROOT_FOLDER

cp -a ../../WebLogo/bin-debug/fonts $ROOT_FOLDER
cp -a ../../WebLogo/bin-debug/images $ROOT_FOLDER
cp -a ../../WebLogo/bin-debug/javascript/* $SRC_FOLDER
cp -a ../../WebLogo/bin-debug/style/* $STYLE_FOLDER
cp -a ../../WebLogo/bin-debug/effects.js $SRC_FOLDER

mkdir $SRC_FOLDER/touchpunch
mkdir $SRC_FOLDER/howler
mkdir $SRC_FOLDER/post
mv $SRC_FOLDER/touchpunch.js $SRC_FOLDER/touchpunch/.
mv $SRC_FOLDER/howler.js $SRC_FOLDER/howler/.
mv $SRC_FOLDER/post.js $SRC_FOLDER/post/.

mkdir $STYLE_FOLDER/bootstrap
mv $STYLE_FOLDER/bootstrap*.* $STYLE_FOLDER/bootstrap/.

cp -a ../../WebLogo/bin-debug/bootstrap.min.js $SRC_FOLDER/bootstrap/.

#################################
# style folder needs to contain styling of both WebLogo and ScriptBlocks.
# wlwidgets styling is in its own folder
cp -a ../../scriptblocks/style/* $STYLE_FOLDER
#################################
cd ../../WebLogo/src/ts
make app
cd ../../../clix/collectstatic
cp -a ../../WebLogo/src/ts/Build/out/app.js $SRC_FOLDER/.

#################################
rm -rf $DEST_FOLDER
mkdir $DEST_FOLDER
cp -a $FOLDER/* $DEST_FOLDER/.
rm -rf $FOLDER
