from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
	url(r'^editor/', include('sln_editor.urls')),
	url(r'^api/', include('sln_editor.api')),
)
