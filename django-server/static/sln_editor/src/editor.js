console = window.console || { log: function() {} };

////////////////////////
// StarLogo Nova Lite specific variables
slnova.lite = {};
slnova.lite.PROJECT_URL = "/api/project/";
slnova.lite.PROJECTLIST_URL = "/api/projects/";

slnova.lite.projectID = -1;
slnova.lite.isLocked = false;
slnova.lite.isProjectLoaded = false;


/**
 * Startup the GUI when the document has finished loading
 */
$(document).ready(function() {
	////////////////////////
	// StarLogo Nova Lite - remove unsupported blocks from drawers
	for(var i = sb.BlockSpecs.drawersJSON.length - 1; i >= 0; i--) {
		if(sb.BlockSpecs.drawersJSON[i].name == "Sound") {
			sb.BlockSpecs.drawersJSON.splice(i, 1);
			continue;
		}
		var index = sb.BlockSpecs.drawersJSON[i].blocks.indexOf("shape-asset");
		if(index > -1) {
			sb.BlockSpecs.drawersJSON[i].blocks.splice(index, 1);
		}
	}
	// add event handlers
	var elSpeedSlider = document.getElementById("speedSlider");
	elSpeedSlider.oninput = slnova.lite.changeSpeedSlider;
	elSpeedSlider.onchange = slnova.lite.changeSpeedSlider;
		
	document.getElementById("id_title").onblur = function() {
		slnova.lite.setDirtyBit(true);	
	}
	document.getElementById("d").onblur = function() {
		slnova.lite.setDirtyBit(true);	
	}


	// override the blockChanged function to add styling for StarLogo Nova Lite
	var oldBlockChanged = sb.WebLogoDemo.blockChanged;
	sb.WebLogoDemo.blockChanged = function() {
		oldBlockChanged();
		if(slnova.lite.isProjectLoaded) {
			slnova.lite.setDirtyBit(true);
		}
	};

	////////////////////////
	// Proceed with usual startup
	sb.WebLogoDemo.init();
	sb.WebLogoDemo.run();

	WidgetManager = slnova.WidgetSpace.getInstance();
	WidgetManager.createDom();

	try {
		if (render_mode == "3d")
			viewport = new Viewport('container');
		else
			viewport = new Viewport2D('container');
	}
	catch(e) {
		console.error('Exception while creating viewport.  ', e);
		if (e == Viewport.prototype.WEBGL_NOT_SUPPORTED || e == Viewport.prototype.INSTANCING_NOT_SUPPORTED)
		{
			if (window.confirm("Your hardware does not support 3D mode. Press OK to load project in 2D mode."))
				window.location.href = window.location.href+"?2d=true";
		}
	}

	// Listen for when widget edit mode is switched, and enable/disable
	// camera controls accordingly.
	var oldEditMode = WidgetManager.setEditMode;
	WidgetManager.setEditMode = function(value) {
		oldEditMode.call(WidgetManager, value);
		// since we don't currently have change events for widgets,
		// just indicate that something has changed whenever edit mode changes.
		slnova.lite.setDirtyBit(true);
	};	
	// edit mode starts as false, so start camera controls as false too.
	viewport.toggleControls(false);

	Helper.KeyManager.init();
	Helper.Utils.init();
	viewport.setAgentStates(Execution.AgentController.states);
	viewport.setAgentPrevStates(Execution.AgentController.prevStates);
	viewport.animate();
	
    var elSpeedSlider = slnova.UIFactory.getInstance().makeToggleButton('pause', 'play', '', '', 'Pause or resume simulation', true);
	elSpeedSlider.id = "speedSliderButton";
	document.getElementById("speedSlider").appendChild(elSpeedSlider);
	elSpeedSlider.addEventListener("click", function(e) {
		if (slnova.UIFactory.getInstance().isToggled(elSpeedSlider)) {
			viewport.setExecRate(document.getElementById('slider').value);
		} else {
			viewport.setExecRate(0);
		}
	}, false, this);
    

	// Make the scriptBlocks drawer and cut/copy/trash toolbar sticky when the user scrolls down
	$('.sbDrawerHolder').addClass('sbSticky');
	$('.sbToolBar').addClass('sbSticky');

	var logoHeight = 65; // $('#logo').height();  on Safari getting the value dynamically on ready event was not accurate
	var navbarHeight = 49; //$('#toolbar').height(); on Safari getting the value dynamically on ready event was not accurate
	var topDrawerHolder = $('.sbDrawerHolder').offset().top - logoHeight;
	var topToolBar = $('.sbToolBar').offset().top - navbarHeight;

	// Implementation of sticky drawers in ScriptBlocks
	function onScroll(event) {
		// Y-Position of Scroll
		var yPos = $(this).scrollTop();
		var bottomFooter = $('footer').offset().top;
		// needs to be 6 - the bottom margin of the pageHolder plus one because the page top is offset by -1
		// no sensible way to calculate it from styling
		var bottom = 6;

		// if we've scrolled down so the footer is visible...
		if((yPos + $(window).height()) > bottomFooter) {
			// ... then subtract the visible portion of the footer so the drawer doesn't overlap the footer
			bottom += ((yPos + $(window).height()) - bottomFooter);
		}
		// if we've scrolled down so that the top of the drawer is passing under the logo
		if (yPos > topDrawerHolder) {
			if(!$('.sbDrawerHolder').hasClass('sbFixed')) {
				// ... then stick the drawer at that position
				$('.sbDrawerHolder').css({'top' : logoHeight,
										'left' : $('.sbDrawerHolder').offset().left});
				// 1 of 2 $('.sbDrawerHolder').css({'height' : ''});
				$('.sbDrawerHolder').addClass('sbFixed');
			}
			$('.sbDrawerHolder').css({'bottom' : bottom});
		} else {
			if($('.sbDrawerHolder').hasClass('sbFixed')) {
				// otherwise let the drawer return to it's natural position
				$('.sbDrawerHolder').removeClass('sbFixed');
				$('.sbDrawerHolder').css({'top' : '', 'left' : '', 'bottom' : ''});
			}
			// changing the height of the drawerholder while scrolling is technically more correct
			// but flickers badly
			// 2 of 2 $('.sbDrawerHolder').css({'height' : yPos + $(window).height() - $('.sbDrawerHolder').offset().top - bottom});
		}
		// the cut/copy/trash toolbar aligns under the header NOT the logo
		if (yPos > topToolBar) {
			$('.sbToolBar').addClass('sbFixed');
			$('.sbToolBar').css({'top' : navbarHeight,
				'right' : ($(window).width() - ($('.sbPageHolder').offset().left + $('.sbPageHolder').outerWidth()))
			});
		} else {
			$('.sbToolBar').removeClass('sbFixed');
			$('.sbToolBar').css({'right':'', 'top' : ''});
		};
	}

	$(window).scroll(onScroll);

	// Sets longest width for .sbDrawerHolder
	function longestWidth() {
		var longestBlockWidth = 0;
		var $sbBlock;
		$('.sbBlockLayoutParent').each(function(){
			$sbBlock = $(this);
			if ($sbBlock.width() > longestBlockWidth) {
				longestBlockWidth = $sbBlock.width();
			}
		});
		return longestBlockWidth;
	}

	// Resizable DrawerHolder & Function to Limit its Stretch:
	$('.sbDrawerHolder').resizable({
		handles: 'e',
		alsoResize: '.sbPageHolder',
		maxWidth: longestWidth() + 25, // 25 is added to add a small amount of arbitrary space between the DrawerHolder's edge and its blocks
		resize: function( event, ui ) {
			// Force script window to resize
			sb.WebLogoDemo.workspace.resize(true);
			onScroll();
			//console('onscroll');
		},
		stop: function( event, ui ) {
			// Force script window to resize
			sb.WebLogoDemo.workspace.resize(true);
			onScroll();
		}
	});

	////////////////////////
	// StarLogo Nova Lite - load the selected project or ask user to pick a project
	var project_id = slnova.lite.getUrlParameter('id', -1);
	console.log('project_id: ' + project_id);
	// Taking out the original isNaN() check on the ``project_id``
	//   because some database backends use alphanumeric IDs,
	//   like MongoDB (i.e. 5aba9f719adabc513b41805e).
	// if(!isNaN(project_id) && project_id != -1) {
	if(project_id !== "" && project_id != -1) {
		slnova.lite.openProject(project_id);
	} else {
		slnova.lite.displayProjectPicker();
	}
});

/**
 * enableCameraControl - called in widgetSpace.js
 */
function enableCameraControl(enable) {
	viewport.toggleControls(enable);
}

/**
 * Changes the speed of the simulation in response to a slider event
 */
slnova.lite.changeSpeedSlider = function() {
	var speed = document.getElementById('slider').value;
	document.getElementById('speedSliderValue').innerHTML = speed;
	viewport.setExecRate(speed);
};

/**
 * Handles unsaved changes
 */
slnova.lite.setDirtyBit = function(on) {
	
	var elSave = document.getElementById(slnova.lite.isLocked ? 'remixLi' : 'manualAuto');
	
	if(on) {
		elSave.classList.add("dirtyBit");
	} else {
		elSave.classList.remove("dirtyBit");
	}
	sb.WebLogoDemo.needsSave = on;
};

/**
 * Helper function to format created_at and saved_at fields
 * @param timeStamp
 * @returns {String}
 */
slnova.lite.formatTimeStamp = function(timeStamp) {
	var date = new Date(timeStamp);
	return date.toLocaleDateString() + ", " + date.toLocaleTimeString();
};

/**
 * Converts a 'true' or 'false' string to a boolean value
 * @param val
 * @returns
 */
slnova.lite.processBoolean = function(val) {
	if(val && typeof val == "string") {
		if(val.toLowerCase() === "true") {
			return true;
		}
		if(val.toLowerCase() === "false") {
			return false;
		}
	}
	return val;
};

/**
 * Returns the value for paramName from the URL or defaultValue if not present
 * @param paramName
 * @param defaultValue
 * @returns
 */
slnova.lite.getUrlParameter = function(paramName, defaultValue ) {
	var definitions = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ) ).split( '&' );

	for (var i = 0; i < definitions.length; i++) {
		def = definitions[i].split('=');
		if(def.length == 2) {
			if(def[0] == paramName) {
				return slnova.lite.processBoolean(def[1]);
			}
		}
	}
	return defaultValue;
}

/**
 * Changes the URL to set paramName to value without refreshing page
 * @param paramName
 * @param value
 */
slnova.lite.setUrlParameter = function(paramName, value ) {
	if(window.history.replaceState) {
		var paramIndex = window.location.href.indexOf( '?' );
		var newURL = window.location.href + "?";
		if(paramIndex > -1) {
			newURL = window.location.href.slice(0, paramIndex) + "?";
			var definitions = decodeURIComponent( window.location.href.slice( paramIndex + 1 ) ).split( '&' );

			for (var i = 0; i < definitions.length; i++) {
				def = definitions[i].split('=');
				if(!(def.length == 2 && def[0] == paramName)) {
					newURL += definitions[i] + '&';
				}
			}
		}
		newURL += paramName + '=' + value;
		window.history.replaceState(null, null, newURL);
	}
};

/////////////////////////////////////////////////////
// SERVER COMMUNICATION
/**
 * GET the project from the server and load it into the GUI
 * @param project_id
 */
slnova.lite.openProject = function(project_id) {
	var urlParam = slnova.lite.PROJECT_URL + project_id.toString() + "/";
	console.log('calling GET on: ' + urlParam);
	$.ajax({
		type : "GET",
		url : urlParam,
		crossDomain : true,
		success : function(response) {
			slnova.lite.setUrlParameter('id', response.id);
			slnova.lite.projectID = response.id;
			slnova.lite.isLocked = slnova.lite.processBoolean(response.is_locked);
			slnova.lite.loadProject(response);},
		error: function(response) {
			console.log('Could not find project at:' + urlParam);
			console.log(response);
			slnova.lite.displayProjectPicker();
		}
	});
};

/**
 * Load a project str into the empty GUI
 * @param projectJSON
 */
slnova.lite.loadProject = function(projectJSON) {
	console.log('loading project: ', projectJSON);
	var elTitle = document.getElementById("id_title");
	elTitle.value = document.title = projectJSON.title;

	var elDescription = document.getElementById("d");
	elDescription.value = projectJSON.description;

	var elCreatedAt = document.getElementById("createdAt");
	elCreatedAt.textContent = slnova.lite.formatTimeStamp(projectJSON.created_at);

	var elSavedAt = document.getElementById("savedAt");
	elSavedAt.textContent = slnova.lite.formatTimeStamp(projectJSON.saved_at);

	console.log("Attempting to load project in djangoweblogo");
	sb.WebLogoDemo.loadProject(projectJSON.project_str, function(){
		console.log("done loading project");
		slnova.lite.isProjectLoaded = true;
		
		if(slnova.lite.isLocked) {
			document.getElementById("manualAutoBtn").classList.add("fa");
			document.getElementById("manualAutoBtn").classList.add("fa-lock");
		}		
		updateShapeList();
		
		document.getElementById("remixLi").onclick = slnova.lite.remixProject;
		document.getElementById("newProjectLi").onclick = slnova.lite.newProject;
		document.getElementById('manualAuto').onclick = function() { slnova.lite.saveProject(slnova.lite.projectID);};
	});
};

/**
 * Generate a JSON object representing the project and send it to server
 */
slnova.lite.saveProject = function(projectID) {
	if(projectID == slnova.lite.projectID && slnova.lite.isLocked) {
		slnova.UIFactory.getInstance().showFadeMessage("\tProject is locked.  To save your changes, you must remix.");
		return;
	};
	var elStatus = document.getElementById("statusBarLi");	
	var data = {
		"title" : (projectID == slnova.lite.projectID ? "" : "Copy of ") + document.getElementById("id_title").value,
		"description" : document.getElementById("d").value,
		"project_str" : sb.WebLogoDemo.wljson.getProjectJSON(),
	};
	var urlParam = slnova.lite.PROJECT_URL + projectID + '/';

	elStatus.textContent = "Saving...";
	$.ajax({
			type : "PATCH",
			url : urlParam,
			crossDomain : true,
			contentType: "application/json",
			data: JSON.stringify(data),
			success : function(project){
				sb.WebLogoDemo.needsSave = false;
				if(projectID != slnova.lite.projectID) {
					window.location.href = "/editor/?id=" + projectID;
				} else {
					var elSavedAt = document.getElementById("savedAt");
					elSavedAt.textContent = slnova.lite.formatTimeStamp(project.saved_at);
					elStatus.textContent = "Saved at " + elSavedAt.textContent;
					slnova.lite.setDirtyBit(false);
					setTimeout(function(){
						elStatus.textContent = "";
					}, 1000);
				}
			},
			error: function(response) {
				elStatus.textContent = "Error while " + (slnova.lite.projectID == projectID ? "saving" : "remixing");
				console.log('Got error when patching data to: ' + urlParam, response);
				console.log(response);
			}
		});
};

/**
 * Make a copy of the current project on the server and redirect the page to the new project
 */
slnova.lite.remixProject = function() {
	var data = {
	};
	var urlParam = slnova.lite.PROJECT_URL + slnova.lite.projectID + "/remixes/";
	$.ajax({
			type : "POST",
			url : urlParam,
			crossDomain : true,
			contentType: "application/json",
			data: JSON.stringify(data),
			success : function(response) {
				slnova.lite.saveProject(response.id);
			},
			error: function(response) {
				document.getElementById("statusBarLi").textContent = "Error while remixing";
				console.log('Got error when remixing project via url: ' + urlParam, response);
			}
		});
};

/**
 * Create a new project on the server and open the new project if no project is already open OR
 * 		redirect the browser to the new project's URL
 */
slnova.lite.newProject = function() {
	console.log('user clicked new project');
	var data = {
	};
	var urlParam = slnova.lite.PROJECTLIST_URL;
	$.ajax({
			type : "POST",
			url : urlParam,
			crossDomain : true,
			contentType: "application/json",
			data: JSON.stringify(data),
			success : function(response) {
				if(slnova.lite.projectID == -1) {
					slnova.lite.projectID = response.id;
					slnova.lite.setUrlParameter('id', response.id);
					slnova.lite.loadProject(response);
				} else {
					window.location.href = "/editor/?id=" + response.id;
				}
			},
			error: function(response) {
				document.getElementById("statusBarLi").textContent = "Error while creating a new project ";
				console.log(document.getElementById("statusBarLi").textContent + urlParam, response);
			}
		});
};

/**
 * Retrieve project list from server and display them in a prompt
 */
slnova.lite.displayProjectPicker = function() {
	var paramPos = window.location.href.indexOf('?');
	var urlParam = slnova.lite.PROJECTLIST_URL + (paramPos > -1 ? window.location.href.slice(paramPos) : '');

	$.ajax({
			type : "GET",
			url : urlParam,
			crossDomain : true,
			contentType: "application/json",
			success : function(projectList) {
				try {
					slnova.lite.loadProjectPicker(projectList);
				} catch(err) {
					console.error('Unexpected data from ' + urlParam);
				}
			},
			error: function(response) {
				document.getElementById("statusBarLi").textContent = "Error while fetching project list from server ";
				console.log(document.getElementById("statusBarLi").textContent + urlParam, response);
			}
		});
};

/**
 * Populate the project picker with the projectList and display it
 * @param projectList
 */
slnova.lite.loadProjectPicker = function(projectList) {
	var elPickProjectGUI = document.getElementById("slnLitePickProject");
	var elDlg = document.getElementById("slnLitePickProjectDlg");
	var elList = document.getElementById("slnLiteProjectList");
	elList.innerHTML = '';
	
	var elNewProject = document.getElementById('slnLiteNewProject');
	elNewProject.onclick = function() {
			elPickProjectGUI.classList.add('sbHidden');
			slnova.lite.newProject();
	};
	if(projectList.length <= 0){
		document.getElementById("slnLitePickProjectPrompt").classList.add("sbHidden");
	} else {
		document.getElementById("slnLitePickProjectPrompt").classList.remove("sbHidden");
	}
	projectList.forEach(function(project){
		var elProject = document.createElement('li');
		
		var elIcon = document.createElement('span');
		elIcon.className = "fa slnlite-project-icon";
		if(project.is_locked) {
			elIcon.classList.add("fa-lock");
		} else {
			
		}
		elProject.appendChild(elIcon);
		
		var elText = document.createElement('span');
		elText.textContent = project.title;
		elText.classList.add("slnlite-project-title");
		
		elProject.appendChild(elText);
		elProject.classList.add('slnlite-project');
		elList.appendChild(elProject);
		elProject.onclick = function() {
			elPickProjectGUI.classList.add('sbHidden');
			setTimeout(function() {
				console.log('calling openProject for project: ' + project.id);
				slnova.lite.openProject(project.id);
			}, 25);
		};
	});
	elPickProjectGUI.classList.remove('sbHidden');
	slnova.UIFactory.getInstance().center(elDlg);
};

/**
 * Global Variables used elsewhere in StarLogo Nova
 * previously declared in djangoweblogo.js
 */
var viewport;
//var render_mode = (slnova.lite.getUrlParameter('2d', false) ? '2d' : '3d');
var render_mode = '2d';
var isLatestRevision = false;

/**
 * Global Variables used elsewhere in StarLogo Nova
 * previously declared in various asset related files which are not included in StarLogo Nova Lite
 */
var AssetManager = {};
AssetManager.getProjectAssets = function() { return [];};
var PRELOAD_PANEL_STACK = "preloading";
const TRANSFORM_FILE_EXTENSION = "slb";
const SUPPORTED_MODEL_FORMATS = ["dae", "obj"];

