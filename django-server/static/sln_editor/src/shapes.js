
function updateShapeList() {

	var beginURL = '/static/sln_editor/shapes/';
	var directory = "animals/";
	var animalModels = [{'label':'Angelfish', 'value':beginURL+directory+"angel-fish/angel-fish.obj"},
	{'label':'Bear', 'value':beginURL+directory+"bear/bear.obj"},
	{'label':'Clownfish', 'value':beginURL+directory+"clownfish/clownfish.obj"},
	{'label':'Clownfish 2', 'value':beginURL+directory+"clownfish-hi-res/clownfish-hi-res.obj"},
	{'label':'Dog', 'value':beginURL+directory+"dog/dog.obj"},
	{'label':'Dragon', 'value':beginURL+directory+"dragon/dragon.obj"},
	{'label':'Elephant', 'value':beginURL+directory+"elephant/elephant.obj"},
	{'label':'Giraffe', 'value':beginURL+directory+"giraffe/giraffe.obj"},
	{'label':'Killer Whale', 'value':beginURL+directory+"killer-whale/killer-whale.obj"},
	{'label':'Lion', 'value':beginURL+directory+"lion/lion.obj"},
	{'label':'Lioness', 'value':beginURL+directory+"lioness/lioness.obj"},
	{'label':'Manta Ray', 'value':beginURL+directory+"mantaray/mantaray.obj"}, //possibly oriented wrong
	{'label':'Otter 1', 'value':beginURL+directory+"otter/otter-float.obj"},
	{'label':'Otter 2', 'value':beginURL+directory+"otter/otter-stand.obj"},
	{'label':'Penguin', 'value':beginURL+directory+"penguin/penguin.obj"},
	{'label':'Raccoon', 'value':beginURL+directory+"raccoon/raccoon.obj"},
	{'label':'Sea Turtle', 'value':beginURL+directory+"sea-turtle/sea-turtle.obj"},
	{'label':'Seagull', 'value':beginURL+directory+"seagull/seagull.obj"},
	{'label':'Seahorse', 'value':beginURL+directory+"seahorse/seahorse.obj"},
	{'label':'Shark', 'value':beginURL+directory+"shark/shark.obj"},
	{'label':'Tuna', 'value':beginURL+directory+"tuna/tuna.obj"},
	{'label':'Turtle', 'value':beginURL+directory+"simpleturtle/simpleturtle.obj"},
	{'label':'Turtle 2', 'value':beginURL+directory+"turtle2/turtle2.obj"},
	{'label':'Urchin', 'value':beginURL+directory+"urchin/urchin.obj"}];
	
	directory = "basic-shapes/";
	var shapeModels = [{'label':'Cone', 'value':beginURL+directory+"cone/cone.obj"},
	{'label':'Cube', 'value':beginURL+directory+"cube/cube.obj"},
	{'label':'Cylinder', 'value':beginURL+directory+"cylinder/cylinder.obj"},
	{'label':'Diamond', 'value':beginURL+directory+"diamond/diamond.obj"},
	{'label':'Pyramid', 'value':beginURL+directory+"pyramid/pyramid.obj"},
	{'label':'Sphere', 'value':beginURL+directory+"sphere/sphere.obj"},
	{'label':'Tetrahedron', 'value':beginURL+directory+"tetrahedron/tetrahedron.obj"},
	{'label':'Torus', 'value':beginURL+directory+"torus/torus.obj"}];
	
	directory = "buildings/";
	var buildingModels = [{'label':'Castle', 'value':beginURL+directory+"castle/castle.obj"},
	{'label':'Castle 2', 'value':beginURL+directory+"freecastle/freecastle.obj"},
	//{'label':'Fortress', 'value':beginURL+directory+"fortress/fortress_textured_sketchup.obj"}, //works but the model is what? o.O
	{'label':'Hospital', 'value':beginURL+directory+"hospital/hospital.obj"},
	{'label':'House', 'value':beginURL+directory+"house/house.obj"}];
	
	directory = "landscape/";
	var landscapeModels = [{'label':'Flower', 'value':beginURL+directory+"flower/flower.obj"},
	{'label':'Grass', 'value':beginURL+directory+"grass/grass.obj"},
	{'label':'Kelp', 'value':beginURL+directory+"kelp/kelp.obj"},
	{'label':'Kelp 2', 'value':beginURL+directory+"kelp/kelp2.obj"},
	{'label':'Rock', 'value':beginURL+directory+"rock/rock.obj"},
	{'label':'Rock 2', 'value':beginURL+directory+"rock/rock2.obj"},
	{'label':'Tree', 'value':beginURL+directory+"tree/tree.obj"}];
	
	directory = "letters/";
	var letterModels = [{'label':'A', 'value':beginURL+directory+"a/a.obj"},
	{'label':'B', 'value':beginURL+directory+"b/b.obj"},
	{'label':'C', 'value':beginURL+directory+"c/c.obj"},
	{'label':'D', 'value':beginURL+directory+"d/d.obj"},
	{'label':'E', 'value':beginURL+directory+"e/e.obj"},
	{'label':'F', 'value':beginURL+directory+"f/f.obj"},
	{'label':'G', 'value':beginURL+directory+"g/g.obj"},
	{'label':'H', 'value':beginURL+directory+"h/h.obj"},
	{'label':'I', 'value':beginURL+directory+"i/i.obj"},
	{'label':'J', 'value':beginURL+directory+"j/j.obj"},
	{'label':'K', 'value':beginURL+directory+"k/k.obj"},
	{'label':'L', 'value':beginURL+directory+"l/l.obj"},
	{'label':'M', 'value':beginURL+directory+"m/m.obj"},
	{'label':'N', 'value':beginURL+directory+"n/n.obj"},
	{'label':'O', 'value':beginURL+directory+"o/o.obj"},
	{'label':'P', 'value':beginURL+directory+"p/p.obj"},
	{'label':'Q', 'value':beginURL+directory+"q/q.obj"},
	{'label':'R', 'value':beginURL+directory+"r/r.obj"},
	{'label':'S', 'value':beginURL+directory+"s/s.obj"},
	{'label':'T', 'value':beginURL+directory+"t/t.obj"},
	{'label':'U', 'value':beginURL+directory+"u/u.obj"},
	{'label':'V', 'value':beginURL+directory+"v/v.obj"},
	{'label':'W', 'value':beginURL+directory+"w/w.obj"},
	{'label':'X', 'value':beginURL+directory+"x/x.obj"},
	{'label':'Y', 'value':beginURL+directory+"y/y.obj"},
	{'label':'Z', 'value':beginURL+directory+"z/z.obj"}];
	
	directory = "numbers/";
	var numberModels = [{'label':'0', 'value':beginURL+directory+"0/0.obj"},
	{'label':'1', 'value':beginURL+directory+"1/1.obj"},
	{'label':'2', 'value':beginURL+directory+"2/2.obj"},
	{'label':'3', 'value':beginURL+directory+"3/3.obj"},
	{'label':'4', 'value':beginURL+directory+"4/4.obj"},
	{'label':'5', 'value':beginURL+directory+"5/5.obj"},
	{'label':'6', 'value':beginURL+directory+"6/6.obj"},
	{'label':'7', 'value':beginURL+directory+"7/7.obj"},
	{'label':'8', 'value':beginURL+directory+"8/8.obj"},
	{'label':'9', 'value':beginURL+directory+"9/9.obj"}];
	
	directory = "objects/";
	var objectModels = [{'label':'Carrot', 'value':beginURL+directory+"carrot/carrot.obj"},
	{'label':'Coin', 'value':beginURL+directory+"coin/coin-silver.obj"},
	{'label':'Coin 2', 'value':beginURL+directory+"coin/coin2.obj"},
	{'label':'Earth', 'value':beginURL+directory+"earth/earth.obj"},
	{'label':'Earth 2', 'value':beginURL+directory+"earthclouds/earthclouds.obj"},
	{'label':'Enzyme', 'value':beginURL+directory+"enzyme/enzyme.obj"},
	{'label':'Fireball', 'value':beginURL+directory+"fireball/fireball.obj"},
	{'label':'Fireball 2', 'value':beginURL+directory+"fireball2/fireball.obj"},
	{'label':'Fireball 3', 'value':beginURL+directory+"fireball2/fireball2.obj"},
	{'label':'Gem', 'value':beginURL+directory+"gem/gem.obj"},
	{'label':'Hydrant', 'value':beginURL+directory+"hydrant/hydrant.obj"},
	{'label':'Moon', 'value':beginURL+directory+"moon/moon.obj"},
	{'label':'Phospholipid', 'value':beginURL+directory+"phospholipid/phospholipid.obj"},
	{'label':'Star', 'value':beginURL+directory+"star/star.obj"},
	{'label':'Starch Molecule', 'value':beginURL+directory+"starch-molecule/starch-molecule.obj"},
	{'label':'Water Molecule', 'value':beginURL+directory+"water-molecule/water-molecule.obj"}];
	
	directory = "people/";
	var peopleModels = [{'label':'Fireman', 'value':beginURL+directory+"fireman/fireman.obj"}];
	
	directory = "vehicles/";
	var vehicleModels = [{'label':'Boat', 'value':beginURL+directory+"boat/boat.obj"},
	{'label':'Helicopter', 'value':beginURL+directory+"helicopter/helicopter.obj"},
	{'label':'Lambo', 'value':beginURL+directory+"lambo/lambo.obj"},
	{'label':'Plane', 'value':beginURL+directory+"plane/plane.obj"},
	{'label':'Spaceship 1', 'value':beginURL+directory+"spaceship/spaceship1.obj"},
	{'label':'Spaceship 2', 'value':beginURL+directory+"spaceship/spaceship2.obj"},
	{'label':'Train', 'value':beginURL+directory+"train/train.obj"},
	{'label':'Truck', 'value':beginURL+directory+"truck/truck.obj"},
	{'label':'Wooden Train', 'value':beginURL+directory+"train_wood/train_wood.obj"}];
	
	directory = "i2camp/";
	var campModels = [{'label':'Platform', 'value':beginURL+directory+"platform.obj"}, //seems like wall
	{'label':'Wall', 'value':beginURL+directory+"wall.obj"}, //seems like platform
	{'label':'Acacia Branch', 'value':beginURL+directory+"acacia-tree-low/acacia-branch.obj"},
	//{'label':'Acacia Trunk 1', 'value':beginURL+directory+"acacia-tree-low/acacia-trunk.obj"}, //is repeat, improperly oriented and no reason to have this
	{'label':'Acacia Trunk 2', 'value':beginURL+directory+"acacia-tree-low/acacia-trunk2.obj"},
	//{'label':'Horizontal Bar', 'value':beginURL+directory+"bar-racket/bar-horizontal.obj"}, //no reason to have this
	//{'label':'Bar Racket', 'value':beginURL+directory+"bar-racket/bar-racket-poser.obj"}, //no reason to have this
	//{'label':'Vertical Bar', 'value':beginURL+directory+"bar-racket/bar-vertical.obj"},  //no reason to have this
	{'label':'Barrel', 'value':beginURL+directory+"barrel-low/barrel-low.obj"},
	//{'label':'Green Grass', 'value':beginURL+directory+"blade-grass-low/blade-grass-green.obj"},  //way to big
	//{'label':'White Grass', 'value':beginURL+directory+"blade-grass-low/blade-grass-white.obj"},  //way to big
	{'label':'Flower Petal', 'value':beginURL+directory+"blade-petal/blade-petal.obj"},
	{'label':'California', 'value':beginURL+directory+"california/california.obj"},
	{'label':'Capsule', 'value':beginURL+directory+"capsule-low/capsule-low.obj"},
	{'label':'Car', 'value':beginURL+directory+"car/car_riviera.obj"},  //improperly oriented
	{'label':'Ghost', 'value':beginURL+directory+"cartoon-ghost/ghost.obj"}, //improperly oriented
	{'label':'Cartoon Girl', 'value':beginURL+directory+"cartoons/girl.obj"}, //would be nice if cartoon-girl had a head and not too ponytails... :P
	//{'label':'???????', 'value':beginURL+directory+"charlie/Spline\\ Object.obj"},  //space is giving me trouble, but no apparent need for this model
	{'label':'Cloud', 'value':beginURL+directory+"cloud-low/cloud-low.obj"},
	{'label':'Silver Coin', 'value':beginURL+directory+"coin/coin-silver.obj"}, //possibly repeat and test-again
	//{'label':'Gold Coin', 'value':beginURL+directory+"coin/coin2.obj"}, //is repeat
	{'label':'Gold Coin', 'value':beginURL+directory+"coin-low-tex/coin-low-tex-gold-uv.obj"},
	{'label':'Creature', 'value':beginURL+directory+"creatures/creature1.obj"}, //possibly mis-oriented??
	{'label':'Rough Cube', 'value':beginURL+directory+"cube-rough/cube-rough.obj"},
	//{'label':'Grass Cube', 'value':beginURL+directory+"cube-rough/cube-rough-grass.obj"}, //model not unlike previous and definitely not grassy
	{'label':'Rounded Cube', 'value':beginURL+directory+"cube-rounded/cube-rounded-low.obj"},
	{'label':'Flat Cylinder', 'value':beginURL+directory+"cylinder-flat/cylinder-flat.obj"},
	{'label':'Daisy', 'value':beginURL+directory+"daisy/daisy.obj"}, //has a head on both sides of her face... but understandable.
	{'label':'Fireball 4', 'value':beginURL+directory+"fireball-low/fireball-low.obj"}, //possibly repeat
	{'label':'Fireball 5', 'value':beginURL+directory+"fireball-low-tex/fireball-low-tex.obj"}, //possibly repeat
	//{'label':'Fireball 6', 'value':beginURL+directory+"fireball2/fireball.obj"}, //is repeat
	//{'label':'Fireball 7', 'value':beginURL+directory+"fireball2/fireball2.obj"}, //is repeat
	{'label':'Fireman', 'value':beginURL+directory+"fireman-male-hires-tex/fireman-male-hires-tex.obj"}, //actually different although very similar; this model is better
	{'label':'Fish', 'value':beginURL+directory+"fish-simple/fish-simple.obj"}, //misoriented
	{'label':'Fly', 'value':beginURL+directory+"fly-low/fly-low.obj"},
	{'label':'Fly 2', 'value':beginURL+directory+"fly-low/fly-low-wings-up.obj"},
	{'label':'Fortress', 'value':beginURL+directory+"fortress/fortress-textured.obj"},
	{'label':'Fortress 2', 'value':beginURL+directory+"fortress/fortress-textured2.obj"},
	{'label':'Fortress 3', 'value':beginURL+directory+"fortress/fortress-textured3.obj"},
	//{'label':'Fortress 4', 'value':beginURL+directory+"fortress/fortress2.obj"}, //same as previous except for minor, minor, small spots
	{'label':'Fortress 4', 'value':beginURL+directory+"fortress-low/fortress-low.obj"},
	//{'label':'Fortress 5', 'value':beginURL+directory+"fortress-low-tex/fortress-low-tex.obj"}, //is repeat
	{'label':'Frog', 'value':beginURL+directory+"frog-jump/frog-jump-low.obj"},  //improperly oriented
	{'label':'Frog 2', 'value':beginURL+directory+"frog-jump/frog-low.obj"}, //improperly oriented
	//{'label':'Frog 3', 'value':beginURL+directory+"frog-jump/frog1.obj"}, //is repeat
	//{'label':'Frog 4', 'value':beginURL+directory+"frog-jump/frog2.obj"}, //is repeat
	{'label':'Frog 3', 'value':beginURL+directory+"frog-low/frog-low.obj"},
	//{'label':'Frog 6', 'value':beginURL+directory+"frog-tex/frog1.obj"}, //is repeat
	{'label':'Frog 4', 'value':beginURL+directory+"frog-tex/frog2.obj"}, //improperly oriented
	{'label':'Frog 5', 'value':beginURL+directory+"frog3/frog1.obj"},
	{'label':'Frog 6', 'value':beginURL+directory+"frog3/frog2.obj"},
	{'label':'Gem', 'value':beginURL+directory+"gem/gem.obj"},
	//{'label':'??????', 'value':beginURL+directory+"girl/20140429170025019.obj"}, //is repeat
	{'label':'Girl', 'value':beginURL+directory+"girl/girl2.obj"},
	{'label':'Girl 2', 'value':beginURL+directory+"girl/dead/20140429170025019.obj"},  //previous model, just lying down... calling this "dead" is morbid... :(
	{'label':'Whiter Grass', 'value':beginURL+directory+"grass-hires-white/grass-hires-white.obj"},
	{'label':'Hana', 'value':beginURL+directory+"hana/hana-front.obj"}, //improperly oriented
	{'label':'Hana 2', 'value':beginURL+directory+"hana/hana-test.obj"},
	//{'label':'Hana 3', 'value':beginURL+directory+"hana/hana2.obj"}, //is repeat and improperly oriented
	{'label':'Happy', 'value':beginURL+directory+"happy/happy.obj"},
	{'label':'I Love Animals', 'value':beginURL+directory+"iloveanimals/iloveanimals.obj"},
	{'label':'Jar 1', 'value':beginURL+directory+"jar1/jar1.obj"},
	{'label':'Jar 2', 'value':beginURL+directory+"jar2/jar2.obj"},
	//{'label':'Kelp', 'value':beginURL+directory+"kelp-low/kelp-low-white.obj"}, //is repeat
	//{'label':'Kelp 2', 'value':beginURL+directory+"kelp-low/kelp-low-white2.obj"}, //is repeat
	{'label':'Key', 'value':beginURL+directory+"key/key2.obj"},
	{'label':'Killer Whale 2', 'value':beginURL+directory+"killer-whale-low-tex/killer-whale-low-tex.obj"},
	{'label':'Killer Whale 3', 'value':beginURL+directory+"killer-whale-low-tex/killer-whale-low-tex-side.obj"}, //it is in fact on its side
	{'label':'Lilypad', 'value':beginURL+directory+"lilypad/lilypad.obj"},
	{'label':'Madge', 'value':beginURL+directory+"madge/madge.obj"}, //missing texture and improperly rotated
	{'label':'Mannequin', 'value':beginURL+directory+"mannequin/mannequin.obj"},
	{'label':'Mine', 'value':beginURL+directory+"mine/mine-low.obj"},
	{'label':'Ocean Floor', 'value':beginURL+directory+"ocean_floor/ocean_floor.obj"},
	{'label':'Ocean Floor 2', 'value':beginURL+directory+"ocean_floor/Terrain_textured.obj"},
	{'label':'Palm Tree', 'value':beginURL+directory+"palm-tree-low/palm-tree-low.obj"},
	{'label':'Person', 'value':beginURL+directory+"person-low/shape.obj"}, //improperly rotated
	//{'label':'Person 2', 'value':beginURL+directory+"person-low-res/person-low-res-base.obj"}, //is repeat
	{'label':'Person 2', 'value':beginURL+directory+"person-low-res/person-low1.obj"}, //improperly rotated
	{'label':'Simple Person', 'value':beginURL+directory+"person-simple/person-simple.obj"},
	{'label':'Tree Stalk', 'value':beginURL+directory+"plant-tree-stalk-low/plant-tree-stalk-low.obj"},
	{'label':'Police Officer', 'value':beginURL+directory+"police-officer/police-officer.obj"},
	{'label':'Projectile', 'value':beginURL+directory+"projectile-low/projectile-low.obj"},
	{'label':'Race Car', 'value':beginURL+directory+"race-car-hires-white/race-car-hires-white.obj"},
	{'label':'Rock', 'value':beginURL+directory+"rock/rock.obj"},
	{'label':'Rock 2', 'value':beginURL+directory+"rock/rock-textured.obj"},
	{'label':'Rock 3', 'value':beginURL+directory+"rock-low/rock-low.obj"},
	{'label':'Rock 4', 'value':beginURL+directory+"rock-low-tex1/rock-low-tex1.obj"},
	{'label':'Rock 5', 'value':beginURL+directory+"rock-low-tex2/rock-low-tex2.obj"},
	{'label':'Sam', 'value':beginURL+directory+"sam/sam.obj"}, //missing texture
	{'label':'Sculpt 1', 'value':beginURL+directory+"sculpt1/sculpt1.obj"},
	{'label':'Sculpt 2', 'value':beginURL+directory+"sculpt2/fig1.obj"},
	{'label':'Sea Turtle 1', 'value':beginURL+directory+"sea-turtle-low/sea-turtle-low.obj"}, //improperly rotated
	{'label':'Sea Turtle 2', 'value':beginURL+directory+"sea-turtle-low/sea-turtle-low-white.obj"}, //improperly rotated and possibly repeat
	{'label':'Shark', 'value':beginURL+directory+"shark-low/shark-low.obj"}, //improperly rotated
	{'label':'Shark 2', 'value':beginURL+directory+"shark-simple/shark-simple.obj"}, //improperly rotated
	//{'label':'???', 'value':beginURL+directory+"sketchup-models/bar_racket_1x3cubes.obj"}, //buffer has 0 size error -- buggy
	{'label':'Sculpt 3', 'value':beginURL+directory+"sketchup-models/sketchup_model_test1.obj"}, 
	//{'label':'???', 'value':beginURL+directory+"sketchup-models/sketchup_model_test2.obj"}, //loads but probably useless
	//{'label':'???', 'value':beginURL+directory+"sketchup-models/sketchup_model_test3.obj"}, //loads but probably useless
	{'label':'Wedge', 'value':beginURL+directory+"sketchup-models/sketchup-model-from-poser.obj"},
	//{'label':'???', 'value':beginURL+directory+"sketchup-models/sketchup-model-from-poser2.obj"},  //is repeat
	{'label':'Sky Sphere', 'value':beginURL+directory+"skydome/skysphere-textured.obj"},
	//{'label':'Skysphere 1', 'value':beginURL+directory+"skydome/skysphere-white.obj"}, //is just a sphere
	{'label':'Hemisphere', 'value':beginURL+directory+"skydome-low/skydome-low.obj"},
	{'label':'Spaceship 3', 'value':beginURL+directory+"spaceship/spaceship1.obj"}, //improperly rotated
	//{'label':'Spaceship 2', 'value':beginURL+directory+"spaceship/spaceship2.obj"}, //is repeat
	{'label':'Spaceship 4', 'value':beginURL+directory+"spaceship-low/spaceship-low.obj"},
	{'label':'Spaceship 5', 'value':beginURL+directory+"spaceship-tex/spaceship1.obj"}, //improperly rotated
	//{'label':'Reptile Sphere', 'value':beginURL+directory+"sphere-reptile/sphere-reptile.obj"}, //just a sphere o.O
	{'label':'Star 2', 'value':beginURL+directory+"star-low/star.obj"}, //star is not the best way to describe this...
	{'label':'California 2', 'value':beginURL+directory+"states/california.obj"}, //'improperly rotated' and possibly repeat
	//{'label':'California 3', 'value':beginURL+directory+"states/california2.obj"}, //is repeat
	{'label':'US States', 'value':beginURL+directory+"states/usa-48.obj"},
	{'label':'Stone Wall', 'value':beginURL+directory+"stone-wall-low-tex/stone-wall-low-tex.obj"},
	{'label':'Stone Wall 2', 'value':beginURL+directory+"stone-wall-low-tex/stone-wall-low-tex2.obj"},
	//{'label':'Stone Wall 3', 'value':beginURL+directory+"textured-cubes/cube-stonewall-poser.obj"}, //just a cube...
	//{'label':'Stone Wall 4', 'value':beginURL+directory+"textured-cubes/cube-stonewall1.obj"}, //just a shaded cube...
	//{'label':'Reptile Sphere 2', 'value':beginURL+directory+"textured-spheres/reptire-sphere.obj"}, //It IS reptire, not reptile.  Doesn't work however
	{'label':'Cracked Sphere', 'value':beginURL+directory+"textured-spheres/sphere-reptile.obj"}, //has barely visible cracks, but the name isn't right...
	{'label':'Reptile Sphere', 'value':beginURL+directory+"textured-spheres/sphere-reptile2.obj"},
	{'label':'Bare Tree', 'value':beginURL+directory+"tree-bare-low/tree-bare-low.obj"},
	//{'label':'Low Tree', 'value':beginURL+directory+"tree-low/tree-low.obj"}, //is repeat
	{'label':'Tron', 'value':beginURL+directory+"tron/tron-cycle-low.obj"},
	{'label':'Tron Cycle', 'value':beginURL+directory+"tron/tron-cyl.obj"},
	{'label':'Tron Wall', 'value':beginURL+directory+"tron/tron-wall.obj"},
	{'label':'Tuna 2', 'value':beginURL+directory+"tuna-fish-low/tuna-low.obj"},
	{'label':'Tuna 3', 'value':beginURL+directory+"tuna-low-tex/tuna-low-tex.obj"}, //oriented wrong
	//{'label':'Virus', 'value':beginURL+directory+"virus/virus-low.obj"},  //is repeat (of mine)
	{'label':'Volcano', 'value':beginURL+directory+"volcano-low/volcano-low.obj"}];
	
	// Modify the shapelist to refer to the local shape files
	sb.ShapeBlockManager.shapeList = sb.WebLogoDemo.getBuiltInShapeNames().concat(animalModels, shapeModels, buildingModels, landscapeModels, letterModels, numberModels, objectModels, peopleModels, vehicleModels, campModels);
	sb.ShapeBlockManager.shapeAssets = function(){ return [];};
};