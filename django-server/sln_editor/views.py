import json

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core import serializers
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response

from sln_editor.models import Project
from sln_editor.serializers import ProjectSerializer
from sln_editor.serializers import ProjectListSerializer
from sln_editor.serializers import ProjectRemixSerializer

######################################################
# Front-end Entry Point
######################################################

def openEditor(request):
	return render(request, 'sln_editor/editor.html')
	
######################################################
# RESTful Interface
######################################################

class ProjectList(generics.ListCreateAPIView):
	queryset = Project.objects.all().order_by('-is_locked', '-saved_at')
	serializer_class = ProjectListSerializer
	
class ProjectDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Project.objects.all()
	serializer_class = ProjectSerializer
	
class RemixList(generics.ListCreateAPIView):
	queryset = Project.objects.all()
	serializer_class = ProjectRemixSerializer
	
	def get_queryset(self):
		# return list of projects whose 'parent' is the id in the url (defined as parent_project in api.py)
		parent_project = self.kwargs['parent_project']
		return Project.objects.filter(parent_project__id=parent_project)
	
	def perform_create(self, serializer):
		# remix the project by copying all the mutable fields
		parent_project = self.kwargs['parent_project']
		orgProject = Project.objects.get(pk=parent_project)
		if "title" not in self.request.data or self.request.data["title"] == "":
			serializer.save(title="Copy of " + orgProject.title, description=orgProject.description, project_str=orgProject.project_str, parent_project=orgProject)
		else:
			serializer.save(description=orgProject.description, project_str=orgProject.project_str, parent_project=orgProject)

