from django.db import models

class Project(models.Model):
	title = models.CharField(max_length=140, default="New Project")
	description = models.CharField(blank=True, max_length=200)
	project_str = models.TextField(blank=True)
	is_locked = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True) 
	saved_at = models.DateTimeField(auto_now=True)
	
	# Linking to the revision the project remixed from
	parent_project = models.ForeignKey('self', null=True, blank=True, editable=False)
	
	def __unicode__(self): # Python 3: def __str__(self):
		return self.title
