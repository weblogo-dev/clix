from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
	
from sln_editor import views

urlpatterns = patterns('',
	# RESTful interface
	# ex: /api/projects
	url(r'projects/$', views.ProjectList.as_view()),
	# ex: /api/project/5
	url(r'project/(?P<pk>\d+)/$', views.ProjectDetail.as_view()),

	# ex: /api/project/5/remixes
	url(r'project/(?P<parent_project>\d+)/remixes/$', views.RemixList.as_view()),
)

urlpatterns = format_suffix_patterns(urlpatterns)
