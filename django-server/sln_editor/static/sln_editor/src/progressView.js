/**
 * Utility script containing factory functions for creating and other functions for managing loading bar panels. Html loaded 
 * for page must include assets/asset_loading_panel.html in the body in order to use this script. The purpose of this script
 * is to allow the number of loading panels in the document to be more dynamic instead of statically declaring all of them
 * in the html for the page document, to make it easier to manage and refer to these similar html elements (no more 
 * getElementById("blah").style = "block"/"none" all over the place), and to remove redundant element nodes in html files 
 * (if all the loading panels were declared in html, they would only differ in id, so they're pretty much clutter). 
 * 
 * Ideas for improvement:
 * - Panel creation argument for width? (For fitting more text).
 * - Allow stacks to set style properties like width, opacity, color, etc of all panels pushed to them.
 * - Implement animations for collapsing and expanding panels. 
 * - Add utility methods to make it easier to place panels and stacks into a div in the document (not sure how much easier
 * 		this can be made than doing it manually unless a style sheet can be passed as an argument).
 */

ProgressView = function() {
	this.loadingPanels = {}; // Map from panel names to visible loading bar panels.
	this.elPanelStack = document.createElement('div');
	this.elPanelStack.classList.add('panelStack');
	this.elPanelStack.classList.add('hidden');
	document.body.appendChild(this.elPanelStack);
};

ProgressView.getInstance = function() {
    if (ProgressView.instance_) {
      return ProgressView.instance_;
    }
    return ProgressView.instance_ = new ProgressView();
};

ProgressView.prototype.setParent = function(elParent) {
	if(elParent) {
		elParent.appendChild(this.elPanelStack);
		this.elPanelStack.classList.remove('panelStack');
		this.elPanelStack.classList.add('centeredPanelStack');
	} else {
		this.elPanelStack.classList.add('panelStack');
		this.elPanelStack.classList.remove('centeredPanelStack');
		document.body.appendChild(this.elPanelStack);		
	}
};

// name: The string name used to refer to the created loading panel. Also becomes the id attribute of the loading panel 
//		outermost element.
// message: A string for the initial message to display above the loading bar. Optional. Default message is "Loading...".

/*
 * Returns an HTML element for a loading panel.
 */
ProgressView.prototype.createLoadingPanel = function(name, message) {
	console.log("!!!!!! createLoadingPanel: " + name + ', message: ' + message);
	
	if(this.loadingPanels[name] && this.loadingPanels[name].getElementsByClassName("message")) {
		this.loadingPanels[name].getElementsByClassName("message")[0].textContent = message;
		return;
	}
	
	var elPanel = document.createElement('div');
	elPanel.classList.add('loadingPanel');
	
	var elMessage = document.createElement('div');
	elMessage.classList.add('message');
	elMessage.textContent = message;
	
	var elProgressBar = document.createElement('div');
	elProgressBar.classList.add('progressBar');

	var elIndicator = document.createElement('div');
	elIndicator.classList.add('barIndicator');
	
	elProgressBar.appendChild(elIndicator);
	elPanel.appendChild(elMessage);
	elPanel.appendChild(elProgressBar);	
	
	this.loadingPanels[name] = elPanel;
	
	this.elPanelStack.appendChild(elPanel);
	this.elPanelStack.classList.remove('hidden');
	
	return elPanel;
};

// name: String used to identify a loading panel.
/*
 * Removes the specified loading panel from the document and removes references to the panel created by the factory.
 * (This means none of the other utility functions can be used to update the panel). 
 */
ProgressView.prototype.destroyLoadingPanel = function(name) {
	if(this.loadingPanels[name]) {
		this.elPanelStack.removeChild(this.loadingPanels[name]);
		delete this.loadingPanels[name];
		if(this.elPanelStack.children.length <=0) {
			this.elPanelStack.classList.add('hidden');
		}
	}
};

// name: String used to identify a loading panel.
// message: String to update message of specified panel.
/*
 * Updates the message of the loading panel specified by name to display the text message.
 */
ProgressView.prototype.updatePanelMessage = function(name, message) {
	//console.log("!!! updatePanelMessage: " + name + " - " + message);
	if(this.loadingPanels[name] && this.loadingPanels[name].getElementsByClassName("message")[0]) {
		this.loadingPanels[name].getElementsByClassName("message")[0].textContent = message;
	}
};

// name: String used to identify a loading panel.
// percentage: A number in the range [0, 100] indicating how much percent full the loading bar should be.
/*
 * Update the progress of the progress bar of the loading panel specified by name to be percentage% complete.
 */
ProgressView.prototype.updatePanelProgress = function(name, percentage) {
	//console.log("!!! updatePanelProgress: " + name + " - " + percentage);
	if(this.loadingPanels[name] && this.loadingPanels[name].getElementsByClassName("message")[0]) {
		this.loadingPanels[name].getElementsByClassName("barIndicator")[0].style.width = percentage+"px";
	}
};
