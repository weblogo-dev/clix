/*
 * The ModelManager2D is responsible for handling all 2D drawing assets (loading, storing, organizing, and creating assets).
 */

// rotGranularity: Rotation granularity (number of discrete rotations represented by each model) to use for models.
/*
 * Constructor for the ModelManager.
 */
ModelManager2D = function (rotGranularity)
{	
	this.builtIn = ['Sphere', 'Cube', 'Pyramid', 'Triangle'];
	this.rotationGranularity = rotGranularity; // Number of discrete rotations represented by each model
	this.modelMap = new Map(); // Stores models.
	this.tagToUrl = new Map(); // Maps tag aliases to model urls.
	this.urlHasTag = new Set(); // Maintains set of all URLs that have a tag alias.
}


// Methods:
ModelManager2D.prototype = 
{
	// Public Interface:
	
	// model: url or tag specifying the sprite source for the model.
	// callback: optional argument for function to be executed when model is ready.
	/*
	 * Returns a model loaded from the given url/tag and calls callback when the model is
	 * ready to be used if callback is defined. 
	 */
	getModel: function (model, callback) 
	{
		const tagRegEx = /asset_3D Model_[0-9]+/;

		// If model is not an unassociated tag.
    	if (!(tagRegEx.test(model) && !this.tagToUrl.has(model)))
		{
			var fileUrl = model;
			var hasPanel = false;
            if (this.tagToUrl.has(model))
            {
            	fileUrl = this.tagToUrl.get(model);
            	hasPanel = true;
            }

			if (!this.modelMap.has(fileUrl))
			{
				lastSlash = fileUrl.lastIndexOf('/');
				firstSlash = fileUrl.indexOf('/');
				ext = fileUrl.lastIndexOf('.');
				var biPanelName = "Built-in_"+fileUrl.substring(firstSlash+1, lastSlash);
				// Check if panel was already made for an uploaded shape
				if (!(hasPanel || this.urlHasTag.has(fileUrl)))
				{
					// Otherwise, make loading panel for built-in shape. Has simpler progress behavior.
					if (!document.getElementById(PRELOAD_PANEL_STACK))
					{
						// Setup progress panel stack for assets being preloaded.
						const MAX_NUM_PANELS = 10;
						var preloading_stack = createPanelStack(PRELOAD_PANEL_STACK, MAX_NUM_PANELS);
						preloading_stack.style.top = "100px";
						preloading_stack.style.right = "0";
						document.getElementById("container").appendChild(preloading_stack);
					}
						
					var loadingPanel = createLoadingPanel(biPanelName, fileUrl.substring(lastSlash+1, ext), 0.3);
					pushPanelToStack(loadingPanel.id, PRELOAD_PANEL_STACK);
				}
				
				var destroyPanelCallback = function ()
					{
						destroyLoadingPanel(biPanelName);
						if (callback)
							callback();
					};
				this.modelMap.set(fileUrl, new Model2D(fileUrl, this.rotationGranularity, destroyPanelCallback));	
			}
			else if (callback)
				callback();
		
			return this.modelMap.get(fileUrl);
		}
		else if (callback)
			callback();
	},

	// model: a model url/tag
	/*
	 * Returns true if model corresponds to a built-in shape.
	 */
	isBuiltIn: function(model) 
	{
		if (this.builtIn.indexOf(model)>-1) return true;
		return false;
	},

	// tag: An alias for url.
	// url: A string representing the url for a model.
	/*
	 * Register tag as an alias for url so that models with tag as their shape will render with the model
	 * loaded by url.
	 */
	associateTagToUrl: function(tag, url) 
	{
        this.tagToUrl.set(tag, url);
        this.urlHasTag.add(url);
    },

    /*
	 * Clears all Models and unassociate all tags.
	 */
	clearModels: function() 
	{
		this.tagToUrl.clear();
		this.modelMap.clear();
		this.urlHasTag.clear();
	},

	// granularity: New rotation granularity for models.
	/*
	 * Set a new rotation granularity for all models.
	 */
	setRotationGranularity: function(granularity)
	{
		this.rotationGranularity = granularity;
		// TODO: Force aleady loaded models to use new granularity.
	}
}