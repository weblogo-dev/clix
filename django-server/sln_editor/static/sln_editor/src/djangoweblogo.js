/**
 * popWindow is called in project_info.html
 */
popWindow = function(link, name, width, height){
	if(typeof(width)==='undefined') width = 500;
	if(typeof(height)==='undefined') height = 300;
	win = window.open(link,name,'width='+width+', height='+height+', titlebar=no,location=no,scrollbars=no,menubar=no');
	win.focus();
};

//the following is from the Django docs to make the AJAX form submissions work
$(document).ajaxSend(function(event, xhr, settings) {
  function getCookie(name) {
    var cookieValue = null;

    if (document.cookie && document.cookie != '') {
      var cookies = document.cookie.split(';');

      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);

        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) == (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }

    return cookieValue;
  }

  function sameOrigin(url) {
    // url could be relative or scheme relative or absolute
    var host = document.location.host;
    // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;

    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
           (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
           // or any other URL that isn't scheme relative or absolute i.e relative.
           !(/^(\/\/|http:|https:).*/.test(url));
  }

  function safeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }

  if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
  }
});

get = function(project, autosave, saveTime, autosaveTime) {
	projectStr = project;

	console.log("variables are populated");

	if (saveTime < autosaveTime && project != autosave && autosave != "" && loggedIn && (editingOwnProject || collaborator)) {
		console.log("project: " + project);
		console.log("autosave: " + autosave);
		//var r = confirm("Your last auto-save is more recent than your latest manual save.\n\n If you'd like to load your most recent auto-save, click 'OK'.\n If you'd like to load your most recent manual save, click 'Cancel'.");
		//if (r) {
			projectStr = autosave;
		//}
	}
	console.log("About to try to inflate project string in djangoWL");
	if (!(projectStr[0] == '{')) {
		projectStr = sb.WebLogoDemo.inflateString(projectStr);
		projectStr = projectStr.replace(/\\\\n/g,"\\n");
	} else {
		projectStr = projectStr.replace(/\n/g, "\\n");
	}
	console.log("Attempting to load project in djangoweblogo: " + projectStr);
	sb.WebLogoDemo.loadProject(projectStr);
};

/**
 * updateTitle is called in project_info.html
 */
function updateTitle() {
	var t = document.getElementById("id_title");
	var title = t.value;
	var dic = {
		"title" : title,
	};
	var to = domain + abs_url + "title/";
	sb.WebLogoDemo.postWithErrorHandling(to, JSON.stringify(dic), "update title",  
				function(response) {
					document.title = title;
				});
};

/**
 * updateDescription is called in project_info.html
 */
function updateDescription() {
	var d = document.getElementById("d");
	var description = d.value;
	var dic = {
		"description" : description,
	};
	var to = domain + abs_url + "description/";
	sb.WebLogoDemo.postWithErrorHandling(to, JSON.stringify(dic), "update description");
};

/**
 * addTag - called from this file
 */
function addTag() {
	if ($('#id_tag').val().trim()) {
		//console.log('non empty tag');
		var tagName = $("#id_tag").val();
		var dic = {
			"tags" : tagName,
		};
		var to = domain + abs_url + "tags/";
		sb.WebLogoDemo.postWithErrorHandling(to, JSON.stringify(dic), "add tag", function (response) {
			document.getElementById('all_tags').innerHTML += response;
			//console.log('addTag, all_tags: ' + document.getElementById('all_tags').innerHTML);
		});
	}
};

/**
 * removeTag - called from html returned from addTag call
 */
function removeTag(obj) {
	//console.log(obj.parentNode.childNodes[1].innerHTML);
	var to = domain + abs_url + "removeTag/";
	var dic = {
		"tags": obj.parentNode.childNodes[1].innerHTML
	};
	sb.WebLogoDemo.postWithErrorHandling(to, JSON.stringify(dic), "remove tag", function (response) {
		document.getElementById('all_tags').removeChild(obj.parentNode);
	});
};

/**
 * updateRating - called from html - page.html, project_info.html, and this file
 */
function updateRating() {
	//console.log(currentRating);
	var rating = 1 - currentRating;
	var dic = {
		"rating" : rating,
	};
	//console.log(dic);
	var to = domain + abs_url + "rate/";
	//console.log(to);
	sb.WebLogoDemo.postWithErrorHandling(to, JSON.stringify(dic), "update rating", 
			function (response) {
				var newScore = parseInt($("#current_score").html()) + parseInt(rating) - currentRating;
				$("#current_score").text(newScore);
				currentRating = rating;
				//console.log("rating " + rating);
				if (rating == 0) {
					$("#ratebtn").replaceWith('<button class="btn" id="ratebtn" onclick="updateRating();" {% if ownProject or collab %}style="display:none"{% endif %}>Like</button>');
				} else {
					$('#ratebtn').replaceWith('<button id="ratebtn" class="btn btn-success" onclick="updateRating();" {% if ownProject or collab %}style="display:none"{% endif %}>Like</button>');
				}
			});
};

var viewport;

$(document).ready(function() {
	
	$('#submit_tag').click(function () {
		addTag();
		$('#id_tag').val('');
	});

	$('#id_tag').keydown(function(e) {
		if (e.keyCode == 13) {
			addTag();
			$('#id_tag').val('');
		}
	});

    sb.WebLogoDemo.init();
    sb.WebLogoDemo.run();
    
    var container = $('#container')[0];
    WidgetManager = slnova.WidgetSpace.getInstance();
    WidgetManager.createDom();
    
    try {
    	if (render_mode == "3d")
    		viewport = new Viewport('container');
    	else
    		viewport = new Viewport2D('container');
    }
    catch(e) {
    	if (e == Viewport.prototype.WEBGL_NOT_SUPPORTED || e == Viewport.prototype.INSTANCING_NOT_SUPPORTED)
    	{
    		if (window.confirm("Your hardware does not support 3D mode. Press OK to load project in 2D mode."))
    			window.location.href = window.location.href+"?2d=true";
    	}
    }

    // Listen for when widget edit mode is switched, and enable/disable
    // camera controls accordingly.
    var oldEditMode = WidgetManager.setEditMode;
    WidgetManager.setEditMode = function(value) {
    	oldEditMode.call(WidgetManager, value);
    	// since we don't currently have change events for widgets,
    	// just indicate that something has changed whenever edit mode changes.
    	sb.WebLogoDemo.needsSave = true;
			sb.WebLogoDemo.enableSaveButton();
    };
    // edit mode starts as false, so start camera controls as false too.
    viewport.toggleControls(false);

    Helper.KeyManager.init();
    Helper.Utils.init();
    viewport.setAgentStates(Execution.AgentController.states);
    viewport.setAgentPrevStates(Execution.AgentController.prevStates);
    viewport.animate();
    
    var elSpeedSlider = slnova.UIFactory.getInstance().makeToggleButton('pause', 'play', '', '', 'Pause or resume simulation', true);
	elSpeedSlider.id = "speedSliderButton";
	document.getElementById("speedSlider").appendChild(elSpeedSlider);
	elSpeedSlider.addEventListener("click", function(e) {
		if (slnova.UIFactory.getInstance().isToggled(elSpeedSlider)) {
			viewport.setExecRate(document.getElementById('slider').value);
		} else {
			viewport.setExecRate(0);
		}
	}, false, this);
    
	$.ajax({
		type : "GET",
		url : full_url,
		dataType : "jsonp",
		crossDomain : true
	});
	
	// Make the scriptBlocks drawer and cut/copy/trash toolbar sticky when the user scrolls down
	$('.sbDrawerHolder').addClass('sbSticky');
	$('.sbToolBar').addClass('sbSticky');
	
	var logoHeight = 65; // $('#logo').height();  on Safari getting the value dynamically on ready event was not accurate
	var navbarHeight = 49; //$('#toolbar').height(); on Safari getting the value dynamically on ready event was not accurate
	var topDrawerHolder = $('.sbDrawerHolder').offset().top - logoHeight;
  	var topToolBar = $('.sbToolBar').offset().top - navbarHeight;
 	
	function onScroll(event) {
		// Y-Position of Scroll
		var yPos = $(this).scrollTop();
		var bottomFooter = $('footer').offset().top;
		// needs to be 6 - the bottom margin of the pageHolder plus one because the page top is offset by -1
		// no sensible way to calculate it from styling
		var bottom = 6;
		
		// if we've scrolled down so the footer is visible...
		if((yPos + $(window).height()) > bottomFooter) {
			// ... then subtract the visible portion of the footer so the drawer doesn't overlap the footer
			bottom += ((yPos + $(window).height()) - bottomFooter);
		}
		// if we've scrolled down so that the top of the drawer is passing under the logo
		if (yPos > topDrawerHolder) {
			if(!$('.sbDrawerHolder').hasClass('sbFixed')) {
				// ... then stick the drawer at that position
				$('.sbDrawerHolder').css({'top' : logoHeight, 
										'left' : $('.sbDrawerHolder').offset().left});				
				// 1 of 2 $('.sbDrawerHolder').css({'height' : ''});
				$('.sbDrawerHolder').addClass('sbFixed');
			}
			$('.sbDrawerHolder').css({'bottom' : bottom});
		} else {
			if($('.sbDrawerHolder').hasClass('sbFixed')) {
				// otherwise let the drawer return to it's natural position
				$('.sbDrawerHolder').removeClass('sbFixed');
				$('.sbDrawerHolder').css({'top' : '', 'left' : '', 'bottom' : ''});
			}
			// changing the height of the drawerholder while scrolling is technically more correct
			// but flickers badly
			// 2 of 2 $('.sbDrawerHolder').css({'height' : yPos + $(window).height() - $('.sbDrawerHolder').offset().top - bottom});
		}
		// the cut/copy/trash toolbar aligns under the header NOT the logo
		if (yPos > topToolBar) {
			$('.sbToolBar').addClass('sbFixed');
			$('.sbToolBar').css({'top' : navbarHeight,
				'right' : ($(window).width() - ($('.sbPageHolder').offset().left + $('.sbPageHolder').outerWidth()))
			});
		} else {
			$('.sbToolBar').removeClass('sbFixed');
			$('.sbToolBar').css({'right':'', 'top' : ''});
		};
	}

	$(window).scroll(onScroll);

	// Sets longest width for .sbDrawerHolder
	function longestWidth() {
		var longestBlockWidth = 0;
		var $sbBlock;
		$('.sbBlockLayoutParent').each(function(){
			$sbBlock = $(this);
			if ($sbBlock.width() > longestBlockWidth) {
				longestBlockWidth = $sbBlock.width();
			}
		});
		return longestBlockWidth;
	}

	// Resizable DrawerHolder & Function to Limit its Stretch:
	$('.sbDrawerHolder').resizable({
		handles: 'e',
		alsoResize: '.sbPageHolder',
		maxWidth: longestWidth() + 25, // 25 is added to add a small amount of arbitrary space between the DrawerHolder's edge and its blocks
		resize: function( event, ui ) {
			// Force script window to resize
			sb.WebLogoDemo.workspace.resize(true);
			onScroll();
			//console('onscroll');
		},
		stop: function( event, ui ) {
			// Force script window to resize
			sb.WebLogoDemo.workspace.resize(true);
			onScroll();
		}
	});
});

/**
 * changeSpeedSlider - called in djangoweblogo.html
 */
function changeSpeedSlider() {
	var speed = document.getElementById('slider').value;
	document.getElementById('speedSliderValue').innerHTML = speed;
	viewport.setExecRate(speed);
}

/**
 * togglePlay - called in djangoweblogo.html
 */
function togglePlay() {
}

/**
 * enableCameraControl - called in widgetSpace.js
 */
function enableCameraControl(enable) {
	viewport.toggleControls(enable);
}
