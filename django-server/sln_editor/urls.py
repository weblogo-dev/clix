from django.conf.urls import patterns, url

from sln_editor import views

urlpatterns = patterns('',
    # ex: /editor/?id=34
    url('', views.openEditor, name='Editor')
)