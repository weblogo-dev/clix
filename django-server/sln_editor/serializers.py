from rest_framework import serializers
from sln_editor.models import Project
	
# Caller gets all fields and can set all editable fields of an individual project
class ProjectSerializer(serializers.ModelSerializer):
	class Meta:
		model = Project

# Caller gets all fields EXCEPT project_str and can push all editable fields for a LIST of projects
class ProjectListSerializer(serializers.ModelSerializer):
	class Meta:
		model = Project
		extra_kwargs = {'project_str': {'write_only': True}}
		
# Caller gets and can push all fields EXCEPT project_str and is_locked when remixing a project
class ProjectRemixSerializer(serializers.ModelSerializer):
	class Meta:
		model = Project
		fields = ('id', 'title', 'description', 'parent_project', 'created_at', 'saved_at')
