# README - StarLogo Nova Lite#

This repository provides a (nearly) fully featured StarLogo Nova front-end with limited reliance on a simplified server API.  The API is a RESTful interface for CRUD functionality to create, edit, save, re-open, and remix StarLogo Nova projects. A demo version of the server has been implemented using Django. Implementors can choose to use the provided Django implementation of the server or write their own version of the server API.

### What is this repository for? ###

This project was developed at MIT's STEP lab for the CLIx project
March 2018

### How do I get set up? ###

1. Download Source Code
	* Check out clix repository from source control
2. (Optional) Create Virtual Environment  
The CLIx django server requires legacy builds of django.  Using a virtual environment is recommended. Open a terminal window, cd to the parent folder containing your clix repository (clix) and run the following commands
	* sudo pip install virtualenv (on Windows, follow http://timmyreilly.azurewebsites.net/python-pip-virtualenv-installation-on-windows/)
	* virtualenv clenv 
	where clenv is a virtual environment name that you choose
	* source clenv/bin/activate  
Now your virtual environment is active
3. Install Server Environment: cd to clix and run
	* sudo pip install -r requirements.txt
	* copy clix/django-server/slnova_editor/settings_default.py to clix/django-server/slnova_editor/settings.py
4. Configure Django Server or skip to step 7 to add the StarLogo Nova Lite application to an existing Django project
	* Generate your own copy of settings.py
		* copy clix/django-server/settings_default.py to clix/django-server/settings.py and change as desired  
		NOTE - the default settings.py uses SQLite.  If you have your own database, you may want to use this instead.
	* Create Database - cd clix/django-server and run
	* python manage.py syncdb
5. Run Server - cd clix/django-server
	* python manage.py runserver
6. Access the Server
	* API is accessible from
		* http://127.0.0.1:8000/api/projects/
		* http://127.0.0.1:8000/api/project/<project_id>/
	* Editor is accessible from
		* http://127.0.0.1:8000/editor/
		* http://127.0.0.1:8000/editor/?id=<project_id>  
	where project_id is the id of a project in the database
7. Include StarLogo Nova Lite in An Existing Django Server  
	StarLogo Nova Lite is a fully functional standalone Django application that can be incorporated into an existing Django project.
	* Copy the sln_editor folder from django_server into your existing Django folder
	* Copy the static folder from django-server into your existing Django folder
	* Copy the templates folder from django-server into your existing Django folder
	* Add custom settings from django-server/slnova/settings.py to your existing settings.py
		* Merge in the entries from INSTALLED_APPS ('django.contrib.staticfiles', 'sln_editor', 'rest_framework')
		* Add the REST_FRAMEWORK settings
		* If necessary, modify the STATIC FILES and/or TEMPLATE_DIRS settings 
	* If necessary, modify your Django installation to include the static and template folders from this repository
	* Add the StarLogo Nova endpoints to your project
		* For example, modify the urls.py file in your project's folder by merging in the list from urls.py file in slnova
	* Add the StarLogo Nova Lite project model to your project's database
		* run python manage.py syncdb
8. Customizing StarLogo Nova Lite
	* Add/remove shapes from the StarLogo Nova Lite built-in shapes
		* Add/remove files to/from the static/sln_editor/shapes folder
		* Add/remove references to those files in the static/sln_editor/src/shapes.js folder
	* Make styling changes to StarLogo Nova Lite by modifying static/sln_editor/style/editor.css
	* Advanced
		* Consider using your existing authorization system for StarLogo Nova Lite app
		* To do this, see the documentation on the REST_FRAMEWORK
		